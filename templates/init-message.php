<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<div class="notice notice-success is-dismissible">
    <p>
        <?php _e('Go to <b>"Tools &rarr; Paperknife"</b> to setup Paperknife', 'tcwp-paperknife' ); ?>
    </p>
</div>