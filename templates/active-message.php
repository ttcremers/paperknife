<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<div class="notice notice-warning is-dismissible">
    <p>
        <?php _e('<b>Warning: </b>Paperknife is enabled! All emails send by Wordpress are opened in a new browser window/tab. Go to <b>"Tools &rarr; Paperknife"</b> to disable', 'tcwp-paperknife' ); ?>
    </p>
</div>