<?php if ( ! defined( 'ABSPATH' ) ) { exit; }

class TCWP_PaperknifeMailCatcher {
    private $settings;

    public function __construct( $settings ) {
        $this->settings = $settings;

        // We can't rely on Wordpress session management
        // as we also need sessions when there's no user available
        if ( ! session_id() ) {
            session_start();
        }

        add_action(
            'phpmailer_init', // Mail blocker
            array( $this, 'disable_php_mailer' ), 9 );
        add_filter(
            'wp_mail', // Mail catcher
            array( $this, 'wp_mail_filter' ), 9 );
        add_action(
            'wp_enqueue_scripts', // Fronted
            array( $this, 'load_mail_poller_scripts' ),
            9
        );
        $at = is_admin() ? 'wp_ajax_paperknife_mailpreview_poller' :
                'wp_ajax_nopriv_paperknife_mailpreview_poller';
        add_action( $at, array( $this, 'ajax_mail_checker' ) );

        // Custom session management
        add_action( 'wp_logout', 'session_destroy' );
        add_action( 'wp_login', 'session_destroy' );
    }

    public function ajax_mail_checker() {
        $value = get_transient( 'tcwp-paperknife-'.session_id() );
        wp_send_json(
            array( "preview_mail" => $value, "s" => session_id() ));
    }

    public function wp_mail_filter( $args ) {
        $current_transient = get_transient(
            esc_sql( 'tcwp-paperknife-'.session_id() ) );
        if ( $current_transient ) {
            array_push($current_transient, $args);
            set_transient( esc_sql( 'tcwp-paperknife-'.session_id() ),
                           $current_transient, 60 * 60 );
        }
        else {
            set_transient( esc_sql( 'tcwp-paperknife-'.session_id() ),
                           array($args), 60 * 60 );
        }
    }

    public function load_mail_poller_scripts() {
        wp_enqueue_script(
            'tcwp-paperknife-mail_poller',
            TCWP_PAPERKNIFE_PLUGIN_URL . 'js/mail_poller.js',
            'jquery', TCWP_PAPERKNIFE_VERSION, true );
        $js_vars = array(
            'ajaxURL'   => admin_url( 'admin-ajax.php' ),
            'ajaxNonce' => wp_create_nonce( 'ques-nonce' )
        );
        wp_localize_script( 'tcwp-paperknife-mail_poller', 'paperknife', $js_vars );
    }

    public function disable_php_mailer( $phpmailer ) {
        if ( $this->settings->shouldSendOutMail() ) {
            return new class extends PHPMailer {
                public function send() {
                    // Not sending anything
                    return true;
                }
            };
        }
        return $phpmailer;
    }
}