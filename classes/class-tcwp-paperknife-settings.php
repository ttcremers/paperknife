<?php if ( ! defined( 'ABSPATH' ) ) { exit; }

class TCWP_PaperknifeSettings {
    // variables for the field and option names
    private $option_name_plugin_enabled = 'tcwp_paperknife_plugin_enable';
    private $option_name_admin_only     = 'tcwp_paperknife_admin_only';
    private $option_name_no_mail        = 'tcwp_paperknife_disable_outgoing_mail';
    private $option_name_init_message   = 'tcwp_paperknife_init_message';

    private $hidden_field_name = 'tcwp_paperknife_submit_hidden';
    private $nonce_field_name  = 'tcwp_paperknife_settings_nonce';
    private $nonce_action      = 'tcwp_paperknife_opener_nonce_action';

    public function __construct() {
        $this->action_hooks();
    }

    public function action_hooks() {
        add_action(
            'admin_menu',
            array($this, 'setup_management_page'), 9 );
        // Admin Logic JS
        add_action(
            'admin_enqueue_scripts',
            array( $this, 'load_admin_scripts' ), 9 );
        add_action(
            'admin_notices',
            array( $this, 'admin_notices' ), 9 );
    }

    public function initialize_settings() {
        update_option( $this->option_name_plugin_enabled, false );
        update_option( $this->option_name_admin_only, 'on' );
        update_option( $this->option_name_no_mail, false);
        // Show initial instruction messages after plugin is enabled
        update_option( $this->option_name_init_message, true);
    }

    public function cleanup_settings() {
        delete_option( $this->option_name_plugin_enabled, false );
        delete_option( $this->option_name_admin_only, 'on' );
        delete_option( $this->option_name_no_mail, false);
        delete_option( $this->option_name_init_message, true);
    }

    public function admin_notices() {
        if ( get_option( $this->option_name_plugin_enabled ) == 'on' ) {
            require_once(
                TCWP_PAPERKNIFE_PLUGIN_DIR . 'templates/active-message.php' );
        }
        if ( get_option( $this->option_name_init_message ) ) {
            require_once(
                TCWP_PAPERKNIFE_PLUGIN_DIR . 'templates/init-message.php' );
            update_option( $this->option_name_init_message, false );
        }
    }

    public function load_admin_scripts($hook) {
        if ( $hook === 'tools_page_tcwp_paperknife_settings' ) {
            wp_enqueue_script(
                'tcwp-paperknife-admin',
                TCWP_PAPERKNIFE_PLUGIN_URL . 'js/admin.js',
                'jquery', TCWP_PAPERKNIFE_VERSION, true );
            $js_vars = array(
                'optionPluginEnabled' => $this->option_name_plugin_enabled
            );
            wp_localize_script( 'tcwp-paperknife-admin', 'paperknife', $js_vars );
        }
    }

    public function setup_management_page() {
        add_management_page(
            __( 'Paperknife Options', 'tcwp_paperknife' ),
            __( 'Paperknife', 'tcwp_paperknife' ),
            'manage_options',
            'tcwp_paperknife_settings',
            array( $this, 'settings' ) );
    }

    public function settings() {
        if ( !current_user_can( 'manage_options' ) )  {
            wp_die(
                __( 'You do not have sufficient permissions to access this page.',
                    'tcwp_paperknife' ) );
        }

        // Check and verify POST request when available
        if ( isset( $_POST[ $this->hidden_field_name ] ) &&
             $_POST[ $this->hidden_field_name ] == 'Y' &&
             check_admin_referer( $this->nonce_action, $this->nonce_field_name ) ) {

            // Save the posted value in the database
            update_option(
                $this->option_name_plugin_enabled,
                $_POST[ $this->option_name_plugin_enabled ]
            );
            update_option(
                $this->option_name_admin_only,
                $_POST[ $this->option_name_admin_only ]
            );
            update_option(
                $this->option_name_no_mail,
                $_POST[ $this->option_name_no_mail ]
            );

            // Notify the view template that we saved settings
            $settings_saved = true;
        }

        // Read in existing option values from database
        $data_plugin_enabled = get_option( $this->option_name_plugin_enabled );
        $data_admin_only     = get_option( $this->option_name_admin_only );
        $data_no_mail        = get_option( $this->option_name_no_mail );

        // View Logic
        require_once(
            TCWP_PAPERKNIFE_PLUGIN_DIR . 'templates/settings.php' );
    }

    public function shouldLoadMailPoller() {
        if ( get_option( $this->option_name_plugin_enabled ) === 'on' ) {

            // Only for admin users?
            if ( get_option( $this->option_name_admin_only ) === 'on' ) {
                return current_user_can('administrator');
            } else {
                return true;
            }
        }
        return false;
    }

    public function shouldSendOutMail() {
        return get_option( $this->option_name_plugin_enabled ) &&
            get_option( $this->option_name_no_mail );
    }
}