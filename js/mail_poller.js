(function paperknife_mail_checker() {
  var vs_inbrowser_mail_poller = null;
  var breakTimer = false;

  jQuery.post(
    paperknife.ajaxURL,
    {
      action: 'paperknife_mailpreview_poller',
      nonce: paperknife.ajaxNonce
    },
    function( data ) {
      if ( data.preview_mail ) {
        clearTimeout(vs_inbrowser_mail_poller);
        // The backend manages the que so we don't have to be very precise here
        for (var i = 0, len = data.preview_mail.length; i < len; i++ ) {
          var new_w = open("/vs_inbrowser_mail_preview", "_blank");
          try {
            new_w.focus();
          }
          catch (e) {
            breakTimer = true;
            showDialog("Pop-up Blocker is enabled! Paperknife needs to be able to open popups to show emails on "+ window.location.host +". Please enable popups for this site and hit the GO button.");
            break;
          }
        }
        if ( !breakTimer ) {
          vs_inbrowser_mail_poller = setTimeout(paperknife_mail_checker, 1000);
        }
      }
    }, 'json' );

  function showDialog(txt) {
    var $j = jQuery;

    var $arrow = $j('<div/>').html("&#x21E7;").css('font-size', '60pt');
    var $txt = $j('<span/>').text(txt);
    var $button = $j('<div/>').append(
      $j('<button>GO</button>').css({
        "border": "2px solid #f2f2f2",
        "background-color": "transparent",
        "margin": "2rem 0 2rem 0",
        "font-weight": "normal",
        "padding": ".7rem 7rem .7rem 7rem"
      }).click(function() {
        $dialog.slideUp(function() { $dialog.remove(); });
        paperknife_mail_checker();
      })
    );

    var $dialog = $j('<div/>')
        .append($arrow)
        .append($txt)
        .append($button)
        .css({
          "display": "none",
          "position": 'fixed',
          "z-index": 999999,
          "width": "100%",
          "text-align": "center",
          "font-weight": "bold",
          "padding": "0.2rem",
          "top": "0",
          "background-color": "#7337ff",
          "color": "#fff",
          "-webkit-box-shadow": "0px 2px 8px #000000",
          "-moz-box-shadow": "0px 2px 8px #000000",
          "box-shadow": "0px 2px 8px #000000"
        });
    $j('body').prepend($dialog);
    $dialog.slideDown();
  }
})();
