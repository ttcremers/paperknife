if ( jQuery ) {
  (function($j) {
    var $paperknife_config_rows = $j('tr.paperknife-config');
    var $paperknife_config_input = $paperknife_config_rows.find('input');
    var $enable_check = $j('input#' + paperknife.optionPluginEnabled);
    var paperknife_functions = [
      function() {
        $paperknife_config_input.attr('disabled', 'disabled');
        $paperknife_config_rows.css('opacity', 0.5);
        $enable_check.parent().css('color', 'red');
      },
      function() {
        $paperknife_config_input.removeAttr('disabled');
        $paperknife_config_rows.css('opacity', 1);
        $enable_check.parent().css('color', 'green');
      }];
    $enable_check.change(function(e) {
      paperknife_functions[($j(this).is(':checked') + 0)]();
    });
    paperknife_functions[($enable_check.is(':checked') + 0)]();
  })(jQuery);
}
