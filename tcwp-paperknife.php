<?php if ( ! defined( 'ABSPATH' ) ) { exit; }
/**
 * Plugin Name:     Paperknife
 * Plugin URI:      https://thomascremers.com/paperknife
 * Description:     Preview all Wordpress generated emails in-browser so you don't have to wait and check your email in order to test and verify changes. Every email opens up in its own tab/window.
 * Author:          Thomas T. Cremers
 * Author URI:      https://thomascremers.com
 * Text Domain:     tcwp-paperknife
 * Domain Path:     /languages
 * Version:         0.1.4
 *
 * @package         TCWP_Paperknife
 */
if ( ! class_exists( 'TCWP_Paperknife') ) {
    class TCWP_Paperknife {
        private static $instance;

        public static function instance() {
            if ( ! isset( self::$instance ) &&
                 ! ( self::$instance instanceof TCWP_Paperknife ) ) {

                self::$instance = new TCWP_Paperknife();
                self::$instance->setup_constants();
                self::$instance->includes();

                self::$instance->settings = new TCWP_PaperknifeSettings();

                if ( self::$instance->settings->shouldLoadMailPoller() ) {
                    self::$instance->mail_catcher = new TCWP_PaperknifeMailCatcher(
                        self::$instance->settings);
                }

                self::$instance->action_hooks();

            }
            return self::$instance;
        }

        public function action_hooks() {
            // Plugin install/activation logic
            register_activation_hook(
                __FILE__,
                array( self::$instance, 'plugin_activation' )
            );
            // Plugin deactivation/deinstall(sort of) logic
            register_deactivation_hook(
                __FILE__,
                array( self::$instance, 'plugin_deactivation' )
            );
        }

        public function includes() {
            require_once(
                TCWP_PAPERKNIFE_PLUGIN_DIR .
                'classes/class-tcwp-paperknife-settings.php' );
            require_once(
                TCWP_PAPERKNIFE_PLUGIN_DIR .
                'classes/class-tcwp-paperknife-mail-catcher.php' );
        }

        public function plugin_activation() {
            self::$instance->settings->initialize_settings();
        }

        public function plugin_deactivation() {
            self::$instance->settings->cleanup_settings();
        }

        public function setup_constants() {
            if ( ! defined( 'TCWP_PAPERKNIFE_VERSION' ) ) {
                define(
                    'TCWP_PAPERKNIFE_VERSION',
                    '0.1.4'
                );
            }
            if ( ! defined( 'TCWP_PAPERKNIFE_PLUGIN_DIR' ) ) {
                define(
                    'TCWP_PAPERKNIFE_PLUGIN_DIR',
                    plugin_dir_path( __FILE__ )
                );
            }
            if ( ! defined( 'TCWP_PAPERKNIFE_PLUGIN_URL' ) ) {
                define(
                    'TCWP_PAPERKNIFE_PLUGIN_URL',
                    plugin_dir_url( __FILE__ )
                );
            }
        }
    }
}

// Register/Start Paperknife
function TCWP_Paperknife() {
    global $tcwp_paperknife;
    $tcwp_paperknife = TCWP_Paperknife::instance();
}
TCWP_Paperknife();
